
import 'package:flutter/material.dart';

import '../const_keeper.dart';
import 'colors.dart';

enum AppTheme { Dark , Light }

final Map<AppTheme, ThemeData> appThemeData = {
  AppTheme.Light: ThemeData(
      fontFamily: ConstKeeper.baseFont,
      brightness: Brightness.light,
      scaffoldBackgroundColor: AppColors.appBackgroundColorLight,
      primaryColor: AppColors.primaryColor,
      accentColor: AppColors.accentColor,
      colorScheme: ColorScheme.light(
        primary: AppColors.primaryColorLight,
      ),
      textTheme: _textThemeLight,
      appBarTheme: AppBarTheme(
       textTheme: _textThemeLight,
        titleTextStyle: TextStyle(color: AppColors.primaryTextColorLight,fontSize: 18,fontWeight: FontWeight.bold),
        color: AppColors.appBackgroundColorLight,
        brightness: Brightness.light,
        iconTheme: IconThemeData(
          color: AppColors.iconColorLight,
        ),
      ),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        backgroundColor: AppColors.bottomNavigationColorLight,
        elevation: 16,
        selectedItemColor: AppColors.bottomNavigationSelectedItemColorLight,
        unselectedItemColor: AppColors.bottomNavigationUnSelectedItemColorLight,
        selectedIconTheme: IconThemeData(size: 22),
        unselectedIconTheme: IconThemeData(size: 22),
        selectedLabelStyle: TextStyle(fontSize: 12),
        unselectedLabelStyle: TextStyle(fontSize: 11),
        type: BottomNavigationBarType.fixed,
      )),
  AppTheme.Dark: ThemeData(
    colorScheme: ColorScheme.dark(
      primary: AppColors.primaryColorDark,
    ),
    fontFamily: ConstKeeper.baseFont,
    brightness: Brightness.dark,
    scaffoldBackgroundColor: AppColors.appBackgroundColorDark,
    primaryColor: AppColors.primaryColorDark,
    accentColor: AppColors.accentColorDark,
    textTheme: _textThemeDark,
    appBarTheme: AppBarTheme(
      color: AppColors.appBackgroundColorDark,
      titleTextStyle: TextStyle(color: AppColors.primaryTextColorDark,fontSize: 18,fontWeight: FontWeight.bold),
      brightness: Brightness.dark,
      textTheme: _textThemeDark,
      iconTheme: IconThemeData(
        color: AppColors.iconColorDark,
      ),
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: AppColors.bottomNavigationColorDark,
      elevation: 16,
      selectedItemColor: AppColors.bottomNavigationSelectedItemColorDark,
      unselectedItemColor: AppColors.bottomNavigationUnSelectedItemColorDark,
      selectedIconTheme: IconThemeData(size: 22),
      unselectedIconTheme: IconThemeData(size: 22),
      selectedLabelStyle: TextStyle(fontSize: 12),
      unselectedLabelStyle: TextStyle(fontSize: 11),
      type: BottomNavigationBarType.fixed,
    ),
  ),
};

final TextTheme _textThemeLight = TextTheme(
  headline1: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: -1.5,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w300,
    fontSize: 96.0,
  ),
  headline2: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: -0.5,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w300,
    fontSize: 60.0,
  ),
  headline3: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.0,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 48.0,
  ),
  headline4: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.25,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 34.0,
  ),
  headline5: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.0,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 24.0,
  ),
  headline6: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.15,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w500,
    fontSize: 20.0,
  ),
  subtitle1: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.15,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
  ),
  subtitle2: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.1,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w500,
    fontSize: 14.0,
  ),
  bodyText1: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.6,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
  ),
  bodyText2: TextStyle(
    color: AppColors.primaryTextColorLight,
    letterSpacing: 0.25,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 14.0,
  ),
  button: TextStyle(
    color: Colors.white,
    letterSpacing: 1.25,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 14.0,
  ),
  caption: TextStyle(
    color: AppColors.primaryTextColorLight2,
    letterSpacing: 0.4,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 12.0,
  ),
  overline: TextStyle(
    color: Colors.white,
    letterSpacing: 1.5,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 10.0,
  ),
);

final TextTheme _textThemeDark = TextTheme(
  headline1: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: -1.5,
    fontWeight: FontWeight.w300,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 96.0,
  ),
  headline2: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: -0.5,
    fontWeight: FontWeight.w300,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 60.0,
  ),
  headline3: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.0,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 48.0,
  ),
  headline4: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.25,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 34.0,
  ),
  headline5: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.0,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 24.0,
  ),
  headline6: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.15,
    fontWeight: FontWeight.w500,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 20.0,
  ),
  subtitle1: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.15,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 16.0,
  ),
  subtitle2: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.1,
    fontWeight: FontWeight.w500,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 14.0,
  ),
  bodyText1: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.6,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
  ),
  bodyText2: TextStyle(
    color: AppColors.primaryTextColorDark,
    letterSpacing: 0.25,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 14.0,
  ),
  button: TextStyle(
    color: Colors.white,
    letterSpacing: 1.25,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 14.0,
  ),
  caption: TextStyle(
    color: AppColors.primaryTextColorDark2,
    letterSpacing: 0.4,
    fontWeight: FontWeight.w400,
    fontFamily: ConstKeeper.baseFont,
    fontSize: 12.0,
  ),
  overline: TextStyle(
    color: Colors.white,
    letterSpacing: 1.5,
    fontFamily: ConstKeeper.baseFont,
    fontWeight: FontWeight.w400,
    fontSize: 10.0,
  ),
);
