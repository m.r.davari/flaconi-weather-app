import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColors {
//------------------------- theme Light colors -------------------------------

  static Map<int, Color> colorSwatchThemeLight = {
    50: Color.fromRGBO(80, 15, 233, .1),
    100: Color.fromRGBO(80, 15, 233, .2),
    200: Color.fromRGBO(80, 15, 233, .3),
    300: Color.fromRGBO(80, 15, 233, .4),
    400: Color.fromRGBO(80, 15, 233, .5),
    500: Color.fromRGBO(80, 15, 233, .6),
    600: Color.fromRGBO(80, 15, 233, .7),
    700: Color.fromRGBO(80, 15, 233, .8),
    800: Color.fromRGBO(80, 15, 233, .9),
    900: Color.fromRGBO(80, 15, 233, 1),
  };


  static const Color primaryColorLight = Color(0xff500FE9);
  static const Color accentColorLight = Color.fromRGBO(150, 11, 242, 0.2);

  static const Color appBackgroundColorLight = Color(0xffFFFFFF);
  static const Color primaryTextColorLight = Color(0xFF000000);
  static const Color primaryTextColorLight2 = Color(0xFF969696);


  /// Main colors
  static const Color primaryColor = Color(0xFF500FE9);
  static const Color primaryColorDark = Color(0xFF815BFF);
  static const Color accentColor = Color(0x33966FF2);
  static const Color accentColorDark = Color.fromARGB(1, 80, 15, 233);
  static const Color secondaryColor = Color(0xFF40DDD5);
  static const Color secondaryColorDark = Color(0xFF40DDD5);


  static const Color color1 = Color(0xFF500FE9);
  static const Color color2 = Color(0xFF500FE9);
  static const Color color3 = Color(0xFF500FE9);
  static const Color bg = Color(0xFF500FE9);
  static const Color bg1 = Color(0xFF500FE9);
  static const Color bg2 = Color(0xFF500FE9);
  static const Color bg3 = Color(0xFF500FE9);

  /// Icon color
  static const Color iconColorLight = primaryColor;
  static const Color iconColorDark = Color(0xFFFFFFFF);


  static const Color bottomNavigationColorLight = Color(0xFFFFFFFF);
  static const Color bottomNavigationSelectedItemColorLight = Color(0xFF313131);
  static const Color bottomNavigationUnSelectedItemColorLight =
      Color(0xFF969696);
  static const Color colorCardViewBackgroundColorLight = Color(0xFFF5F5F5);
  static const Color colorAuthButtonLight = Color(0xFF500FE9);
  static const Color colorTextButtonLight = Color(0xFFffffff);

//------------------------- theme Dark colors ----------------------------------

  static Map<int, Color> colorSwatchThemeDark = {
    50: Color.fromRGBO(150, 111, 242, .1),
    100: Color.fromRGBO(150, 111, 242, .2),
    200: Color.fromRGBO(150, 111, 242, .3),
    300: Color.fromRGBO(150, 111, 242, .4),
    400: Color.fromRGBO(150, 111, 242, .5),
    500: Color.fromRGBO(150, 111, 242, .6),
    600: Color.fromRGBO(150, 111, 242, .7),
    700: Color.fromRGBO(150, 111, 242, .8),
    800: Color.fromRGBO(150, 111, 242, .9),
    900: Color.fromRGBO(150, 111, 242, 1),
  };

  static const Color appBackgroundColorDark = Color(0xFF102733);
  static const Color primaryTextColorDark = Color(0xFFFFFFFF);
  static const Color primaryTextColorDark2 = Color(0xFFCCCCCC);

  static const Color bottomNavigationColorDark = Color(0xFFFFFFFF);
  static const Color bottomNavigationSelectedItemColorDark = Color(0xFF313131);
  static const Color bottomNavigationUnSelectedItemColorDark =
      Color(0xFF969696);

  // public single color will add here
  static Color redColor = Color(0xFFEC484A);
  static Color greenColor = Color(0xFF00B947);

  static Color blackTfa = Color(0xff000022);
  static Color greyTfa = Color(0xff76839133);
  static Color lightGreyTfa = Color(0xffF3F5F9);

  static Color tabBarIndicatorLight = primaryColor;
  static Color tabBarIndicatorDark = Color(0xFFEAECEF);

  static Color fieldBg = Color.fromRGBO(0, 0, 0, 0.1);

  static Color lightLoading = Color(0xFF500FE9);
  static Color darkLoading = Color(0xFF815BFF);
}
