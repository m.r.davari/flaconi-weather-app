import 'package:flutter/material.dart';

import 'colors.dart';

extension MultiThemeColorExtension on ThemeData {

  Color get primary => this.brightness == Brightness.light
      ? Color(0xFF500FE9)
      : Color(0xFF815BFF);

  Color get primaryLight => this.brightness == Brightness.light
      ? Color(0xFF5E15EF)
      : Color(0xFF5E15EF);

  Color get secondary => this.brightness == Brightness.light
      ? Color(0xFF40DDD5)
      : Color(0xFF40DDD5);

  Color get secondaryLight => this.brightness == Brightness.light
      ? Color(0xFF43E8E0)
      : Colors.transparent;

  Color get mainBg => this.brightness == Brightness.light
      ? Color(0xffFFFFFF)
      : Color(0xff1A1E25);

  Color get bg => this.brightness == Brightness.light
      ? Color(0xFFFFFFFF)
      : Color(0xFF1E2036);

  Color get bg1 => this.brightness == Brightness.light
      ? Color(0xFFF3F5F9)
      : Color(0xFF23283F);

  Color get bg2 => this.brightness == Brightness.light
      ? Color(0xFFFAFBFF)
      : Color(0x1496A6EF);


  Color get disableFieldBorder => this.brightness == Brightness.light
      ? Color.fromRGBO(118, 131, 145, 0.2)
      : Color.fromRGBO(66, 71, 77, 0.2);

  Color get placeHolderText => this.brightness == Brightness.light
      ? Color(0xff768391)
      : Color(0xff768391);

  Color get fontColor1 => this.brightness == Brightness.light
      ? Color(0xFFFFFFFF)
      : Color(0xFFFFFFFF);

  Color get fontColor2 => this.brightness == Brightness.light
      ? Color(0xFF000022)
      : Color(0xFF000022);

  Color get fontColor3 => this.brightness == Brightness.light
      ? Color(0xffBBBBBB)
      : Color(0xffBBBBBB);

  Color get backgroundColorOpposite => this.brightness == Brightness.light ? Colors.black : Colors.white;


  Color get iconColor => this.brightness == Brightness.light ? this.primary : Colors.white;

  /// Text Field Colors
  Color get textFieldColor => this.brightness == Brightness.light ? Color.fromARGB(1, 0, 0, 0) : Color(0xFF292F49);
  Color get textFieldFocusedBorderColor => this.brightness == Brightness.light ? primary : Colors.transparent;
  Color get textFieldUnfocusedBorderColor => this.brightness == Brightness.light ? Color(0x2E000000) : Colors.transparent;
  Color get textFieldFocusedBackgroundColor => this.brightness == Brightness.light ? Color(0xFFFFFF) : Color(0xFF333A53);
  Color get textFieldUnFocusedBackgroundColor => this.brightness == Brightness.light ? Color(0x03000000) : Color(0xFF292F49);

  Color get dividerPriceColor => this.brightness == Brightness.light ? disableFieldBorder.withOpacity(0.1) : Color(0x1496A6EF);
  /// Button themes
  Color get primaryButtonColor => primary;
  Color get primaryTextButtonColor => this.brightness == Brightness.light ? Color(0xFFFFFFFF) : Color(0xFFEAECEF);
  Color get buttonLoadingColor => this.brightness == Brightness.light ? Color(0xFFFFFFFF) : Color(0xFFEAECEF);

}

