
import 'package:flutter/material.dart';

class ConstKeeper {

  static String baseUrl = 'https://www.metaweather.com/api/';

  static String imageBaseUrl = 'https://www.metaweather.com/static/img/weather/png/';

  static const String baseFont = "IranSans";

}
