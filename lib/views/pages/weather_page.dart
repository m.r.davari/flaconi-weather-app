import 'package:flaconi_weather_app/core/di/locator.dart';
import 'package:flaconi_weather_app/core/livedata/ui_states.dart';
import 'package:flaconi_weather_app/data/location/models/location_entity.dart';
import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_entity.dart';
import 'package:flaconi_weather_app/data/weather/repository/i_weather_repository.dart';
import 'package:flaconi_weather_app/providers/weather_provider/Weather_provider.dart';
import 'package:flaconi_weather_app/views/widgets/modals/setting_modal.dart';
import 'package:flaconi_weather_app/views/widgets/weather/weather_detail_widget.dart';
import 'package:flaconi_weather_app/views/widgets/weather/weather_detail_widget_landscape.dart';
import 'package:flaconi_weather_app/views/widgets/weather/weather_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:provider/provider.dart';


class WeatherPage extends StatefulWidget {
  Location? location;
  String city;
  WeatherPage({Key? key,this.location,required this.city}) : super(key: key);

  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {

  WeatherProvider _weatherProvider = WeatherProvider(locator<IWeatherRepository>());


  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero,(){
      _weatherProvider.getWeatherForeCast(widget.location!.woeid);
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Weather Page'),
        automaticallyImplyLeading: true,
        elevation: 0,
        backgroundColor: Theme.of(context).bg1,
        actions: [
          IconButton(onPressed: (){
            showModalBottomSheet(
              isScrollControlled: MediaQuery.of(context).orientation == Orientation.landscape,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
                ),
                context: context,
                builder: (context) {
                  return SettingModal();
                },
            );
          }, icon: Icon(Icons.settings))
        ],
      ),
      backgroundColor: Theme.of(context).bg1,
      body: ChangeNotifierProvider<WeatherProvider>(
        create: (ctx){
          _weatherProvider.getWeatherForecastLiveData.setValue(Initial());
          return _weatherProvider;
        },
        child: OrientationBuilder(
          builder: (ctx,orientation){
            if(orientation == Orientation.portrait){
              return _bodyWidgetPortrait();
            }
            else{
              return _bodyWidgetLandscape();
            }
          },
        ),
      ),

    );
  }


  Widget _bodyWidgetPortrait(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Container(
        margin: const EdgeInsets.only(top: 4),
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
          color:Theme.of(context).bg,
        ),
        child: Consumer<WeatherProvider>(
          builder: (ctx,providerData,_){
            var state = providerData.getWeatherForecastLiveData.getValue();
            if(state is IsLoading){
              return const Center(child: CircularProgressIndicator(strokeWidth: 2,),);
            }
            else if(state is Success){
              var weatherData = state.data as WeatherForecasts;
              return Column(
                children: [
                  Expanded(
                      child: RefreshIndicator(
                        displacement: 5,
                        backgroundColor: Theme.of(context).bg1,
                        notificationPredicate: (notif) {
                          return true;
                        },
                        triggerMode: RefreshIndicatorTriggerMode.anywhere,
                        onRefresh: ()async{
                          _weatherProvider.getWeatherForeCast(widget.location!.woeid);
                        },
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                          child: WeatherDetailWidget(consolidatedWeather: providerData.getWatherDetailItem,city: widget.city,),
                        ),
                      )
                  ),
                  Container(
                    height: 90,
                    child: AnimationLimiter(
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        physics: BouncingScrollPhysics(),
                        itemCount: weatherData.consolidatedWeather.length,
                        itemBuilder: (ctx,index){
                          return AnimationConfiguration.staggeredList(
                              position: index,
                              duration: const Duration(milliseconds: 425),
                              child: SlideAnimation(
                                horizontalOffset: -60.0,
                                child: FadeInAnimation(
                                  child: WeatherItemWidget(weatherProvider: _weatherProvider, consolidatedWeather: weatherData.consolidatedWeather[index]),
                                ),
                              ),
                          );
                        },
                        separatorBuilder: (ctx,index){
                          return SizedBox(width: 10,);
                        },
                      ),
                    ),
                  )
                ],
              );
            }
            else if(state is Failure){
              return Material(
                color: Colors.transparent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(onPressed: (){
                      _weatherProvider.getWeatherForeCast(widget.location!.woeid);
                    }, icon: Icon(Icons.refresh),iconSize: 28,),
                    SizedBox(height: 16,),
                    Text('${state.error}'),
                  ],
                ),
              );
            }
            else{
              return Container();
            }

          },
        ),
      ),
    );
  }

  Widget _bodyWidgetLandscape(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Container(
        margin: const EdgeInsets.only(top: 4),
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
          color:Theme.of(context).bg,
        ),
        child: Consumer<WeatherProvider>(
          builder: (ctx,providerData,_){
            var state = providerData.getWeatherForecastLiveData.getValue();
            if(state is IsLoading){
              return const Center(child: CircularProgressIndicator(strokeWidth: 2,),);
            }
            else if(state is Success){
              var weatherData = state.data as WeatherForecasts;
              return Row(
                children: [
                  Container(
                    width: 90,
                    child: AnimationLimiter(
                      child: ListView.separated(
                        scrollDirection: Axis.vertical,
                        physics: BouncingScrollPhysics(),
                        itemCount: weatherData.consolidatedWeather.length,
                        itemBuilder: (ctx,index){
                          return AnimationConfiguration.staggeredList(
                            position: index,
                            duration: const Duration(milliseconds: 425),
                            child: SlideAnimation(
                              horizontalOffset: -60.0,
                              child: FadeInAnimation(
                                child: WeatherItemWidget(weatherProvider: _weatherProvider, consolidatedWeather: weatherData.consolidatedWeather[index]),
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (ctx,index){
                          return SizedBox(height: 10,);
                        },
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                      child: RefreshIndicator(
                        displacement: 5,
                        backgroundColor: Theme.of(context).bg1,
                        notificationPredicate: (notif) {
                          return true;
                        },
                        triggerMode: RefreshIndicatorTriggerMode.anywhere,
                        onRefresh: ()async{
                          _weatherProvider.getWeatherForeCast(widget.location!.woeid);
                        },
                        child: Container(
                          height: double.infinity,
                          alignment: Alignment.center,
                          child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                            child: WeatherDetailWidgetLandscape(consolidatedWeather: providerData.getWatherDetailItem,city: widget.city,),
                          ),
                        ),
                      )
                  ),
                ],
              );
            }
            else if(state is Failure){
              return Material(
                color: Colors.transparent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(onPressed: (){
                      _weatherProvider.getWeatherForeCast(widget.location!.woeid);
                    }, icon: Icon(Icons.refresh),iconSize: 28,),
                    SizedBox(height: 16,),
                    Text('${state.error}'),
                  ],
                ),
              );
            }
            else{
              return Container();
            }

          },
        ),
      ),
    );
  }





}
