import 'dart:math';

import 'package:flaconi_weather_app/utils/route_transitions.dart';
import 'package:flaconi_weather_app/views/pages/location_page.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin{


  //flip
  late AnimationController flipController;
  late Animation flip_anim;

  //move up
  late AnimationController transUpController;
  late Animation<Offset> transUpOffset;

  //fade in
  late AnimationController fadeInController;
  late Animation<double> fadeIn ;

  int count = 0 ;

  @override
  void initState() {
    super.initState();


    flipController = AnimationController(duration: const Duration(milliseconds: 1500), vsync: this);
    flip_anim = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(parent: flipController, curve: const Interval(0.0, 1.0, curve: Curves.linear)));

    flip_anim.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if(count<1){
          count++;
          flipController.reset();
          flipController.forward();
        }
        else{
          flipController.stop();
          count=0;
          Future.delayed(const Duration(milliseconds: 300),(){
            Route route = FadeRoute(page: SearchLocationPage());
            Navigator.pushReplacement(context, route);
          });
        }
      }
    });

    transUpController = AnimationController(vsync: this, duration: const Duration(milliseconds: 1500));
    transUpOffset = Tween<Offset>(begin: Offset(0.0, 6.0), end: Offset.zero).animate(transUpController);


    fadeInController = AnimationController(vsync: this,duration: const Duration(seconds: 3));
    fadeIn = Tween<double>(begin: -1.0,end: 1.0).animate(fadeInController);

  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Color(0xFF23283F),
        body: Container(
          width: double.infinity,height: double.infinity,
          //decoration: const BoxDecoration(image: DecorationImage(image: AssetImage("images/img_bg_splash.jpg"),fit: BoxFit.cover)),
          alignment: Alignment.center,
          child: AnimatedBuilder(
              animation: flipController,
              builder: (BuildContext context, Widget? child) {
                transUpController.forward();
                flipController.forward();
                fadeInController.forward();
                return Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                          width: 190,height: 190,
                          alignment: Alignment.center,
                          child: SlideTransition(
                            position: transUpOffset,
                            child: Transform(
                              transform: Matrix4.identity()
                                ..setEntry(3, 2, 0.001)
                                ..rotateY(2 * pi * flip_anim.value),
                              alignment: Alignment.center,
                              child: Container(
                                child: Image.asset('assets/images/flaconi_logo.png',fit: BoxFit.cover,color: Colors.white,),
                              ),
                            ),
                          )
                      ),
                    )
                    ,
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: FadeTransition(
                        opacity: fadeIn,
                        child: const Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text("Flaconi Weather App",style: TextStyle(color: Colors.white,fontSize: 20.0),),
                        ),
                      ),
                    )
                  ],

                );
              }
          ),
        )
    );

  }


  @override
  void dispose() {
    flipController.dispose();
    super.dispose();
  }


}
