import 'package:flaconi_weather_app/core/di/locator.dart';
import 'package:flaconi_weather_app/core/livedata/ui_states.dart';
import 'package:flaconi_weather_app/data/location/repository/i_location_repository.dart';
import 'package:flaconi_weather_app/data/location/models/location_entity.dart';
import 'package:flaconi_weather_app/providers/location_provider/search_provider.dart';
import 'package:flaconi_weather_app/utils/common_utils.dart';
import 'package:flaconi_weather_app/views/widgets/buttons/primary_button.dart';
import 'package:flaconi_weather_app/views/widgets/modals/setting_modal.dart';
import 'package:flaconi_weather_app/views/widgets/textfiels/primary_text_field.dart';
import 'package:flaconi_weather_app/views/widgets/tiles/location_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:provider/provider.dart';




class SearchLocationPage extends StatefulWidget {
  const SearchLocationPage({Key? key}) : super(key: key);

  @override
  _SearchLocationPageState createState() => _SearchLocationPageState();
}

class _SearchLocationPageState extends State<SearchLocationPage> {

  var textEditingController = TextEditingController();
  LocationProvider _searchProvider = LocationProvider(locator<ILocationRepository>());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Location Page'),
        automaticallyImplyLeading: true,
        elevation: 0,
        backgroundColor: Theme.of(context).bg1,
        actions: [
          IconButton(onPressed: (){
            showModalBottomSheet(
              backgroundColor: Theme.of(context).bg,
              isScrollControlled: MediaQuery.of(context).orientation == Orientation.landscape,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
              ),
              context: context,
              builder: (context) {
                return SettingModal();
              },
            );
          }, icon: Icon(Icons.settings))
        ],
      ),
      backgroundColor: Theme.of(context).bg1,
      body: ChangeNotifierProvider<LocationProvider>(
        create: (ctx) {
          _searchProvider.getLocatiosLiveData.setValue(Initial());
          return _searchProvider;
        },
        child: _bodyWidget(),
      ),

    );
  }


  Widget _bodyWidget(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      margin: const EdgeInsets.only(top: 4),
      child: Container(
        padding: const EdgeInsets.all(24),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24)),
          color: Theme.of(context).bg,
        ),
        child: Consumer<LocationProvider>(
          builder: (ctx,data,_){
            UIState state = data.getLocatiosLiveData.getValue();
            return Column(
              children: [
                PrimaryTextField(
                  label: 'Search Location',
                  textEditingController: textEditingController,
                ),
                const SizedBox(height: 16,),
                Expanded(
                  child: _stateHandler(state),
                ),
                const SizedBox(height: 16,),
                PrimaryButton(
                  color: Theme.of(context).primaryColor,
                  onTap: (){
                    hideKeyboardFocus(context);
                    callSearch(textEditingController.text);
                  },
                  text: 'search',
                  isLoading: state is IsLoading,
                )
              ],
            );
          },
        ),
      ),
    );
  }


  Widget _stateHandler(UIState state){
    if (state is IsLoading){
      return Container();
    }
    else if(state is Success){
      var data = state.data as List<Location>;
      if(data.isEmpty){
        return const Center(child: Text('Locations Not Found'),);
      }
      return AnimationLimiter(
        child: ListView.separated(
          physics: const BouncingScrollPhysics(),
          itemCount: data.length,
          itemBuilder: (ctx,index){
            return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 425),
                child: SlideAnimation(
                  horizontalOffset: -60.0,
                  child: FadeInAnimation(
                    child: LocationListTile(location: data[index],)
                  ),
                )
            );
          },
          separatorBuilder: (ctx,index){
            return const SizedBox(height: 10,);
          },
        ),
      );
    }
    else if (state is Failure){
      return Center(child: Text('${state.error}'),);
    }
    else {
      return Container();
    }
  }


  Future<void> callSearch(String searchText) async{

    if(searchText.isEmpty){
      return;
    }

    await _searchProvider.searchLocation(city: textEditingController.text);

  }


}
