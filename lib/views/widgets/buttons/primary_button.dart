import 'package:flaconi_weather_app/resources/theme/color_extension.dart';
import 'package:flaconi_weather_app/resources/theme/colors.dart';
import 'package:flutter/material.dart';
class PrimaryButton extends StatelessWidget {
  final Color color;
  final double height;

  final VoidCallback onTap;
  final bool isLoading;
  final String text;
  final Color textColor;
  final BoxBorder? border;
  final double radius;
  final TextStyle?  textStyle;
  final double? width;

  PrimaryButton(
      {Key? key,
      required this.color,
      this.height = 50.0,
      required this.onTap,
      this.isLoading = false,
      this.text = '',
      this.textColor = AppColors.colorTextButtonLight,
      this.border,
      this.radius = 16,
      this.textStyle,
      this.width,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: border,
      ),
      child: Material(
        color: color,
        borderRadius: BorderRadius.circular(8),
        child: InkWell(
          borderRadius: BorderRadius.circular(8),
          child: Center(
              child: isLoading
                  ? SizedBox(
                      width: 25, height: 25, child: CircularProgressIndicator(
                        color: Theme.of(context).buttonLoadingColor,
                        strokeWidth: 2,
                    ))
                  : Text(text, style: textStyle ?? TextStyle(color: textColor),
              )
          ),
          onTap: isLoading ? null:onTap,
          splashColor: color.withOpacity(0.2),
        ),
      ),
    );
  }
}
