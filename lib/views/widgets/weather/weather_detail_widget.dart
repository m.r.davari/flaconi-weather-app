import 'dart:async';
import 'dart:ui';

import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_entity.dart';
import 'package:flaconi_weather_app/providers/setting_provider/setting_provider.dart';
import 'package:flaconi_weather_app/providers/weather_provider/Weather_provider.dart';
import 'package:flaconi_weather_app/resources/const_keeper.dart';
import 'package:flaconi_weather_app/utils/common_utils.dart';
import 'package:flaconi_weather_app/views/widgets/textfiels/animated_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:provider/provider.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';
import 'package:shimmer/shimmer.dart';

class WeatherDetailWidget extends StatefulWidget {

  ConsolidatedWeather consolidatedWeather;
  String city;
   WeatherDetailWidget({Key? key,required this.consolidatedWeather,required this.city}) : super(key: key);

  @override
  State<WeatherDetailWidget> createState() => _WeatherDetailWidgetState();
}


class _WeatherDetailWidgetState extends State<WeatherDetailWidget> with SingleTickerProviderStateMixin{


  bool shimmerEnable = true;


  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 800),(){
      setState(() {
        shimmerEnable = false;
      });
    });

  }



  @override
  void didUpdateWidget(WeatherDetailWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if(widget.consolidatedWeather.id != oldWidget.consolidatedWeather.id){
      setState(() {
        shimmerEnable = true;
      });
      Future.delayed(Duration(milliseconds: 800),(){
        setState(() {
          shimmerEnable = false;
        });
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return AnimationLimiter(
      child: Column(
        children: [
          Column(
            children: AnimationConfiguration.toStaggeredList(
              duration: const Duration(milliseconds: 375),
              childAnimationBuilder: (widget) => SlideAnimation(
                verticalOffset: -50.0,
                child: FadeInAnimation(
                  child: widget,
                ),
              ),
              children: [
                const SizedBox(height: 8,),
                AnimatedTextWidget(text: '${widget.city}, ${convertDateToDay(widget.consolidatedWeather.applicableDate)}',dayId: widget.consolidatedWeather.id,textStyle: TextStyle(fontSize: 17),),
                const SizedBox(height: 20,),
                Align(
                  alignment: Alignment.centerLeft,
                  child: AnimatedTextWidget(text: '${widget.consolidatedWeather.weatherStateName}',dayId: widget.consolidatedWeather.id,textStyle: TextStyle(fontSize: 15)),
                ),
                const SizedBox(height: 16,),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    FadeInImage(
                        width: 230,height: 230,
                        placeholder: AssetImage('assets/images/place_holder.png',),
                        image: NetworkImage('${ConstKeeper.imageBaseUrl}${widget.consolidatedWeather.weatherStateAbbr}.png',),
                    ),
                    Visibility(
                      visible: shimmerEnable,
                      child: Shimmer.fromColors(child: FadeInImage(
                        width: 230,height: 230,
                        placeholder: AssetImage('assets/images/place_holder.png',),
                        image: NetworkImage('${ConstKeeper.imageBaseUrl}${widget.consolidatedWeather.weatherStateAbbr}.png'),
                      ), baseColor: Colors.transparent, highlightColor: Colors.white,period: Duration(milliseconds: 800),enabled: shimmerEnable,),
                    )

                  ],
                ),
                const SizedBox(height: 18,),
                Text('${Provider.of<SettingProvider>(context).isFahrenheit ? getSubStr(convertCelsiusToFahrenheit(widget.consolidatedWeather.theTemp).toString(),end: 5)+' °F': getSubStr(widget.consolidatedWeather.theTemp.toString(),end: 5)+' °C'}'),
                const SizedBox(height: 18,),
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: AnimationLimiter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: AnimationConfiguration.toStaggeredList(
                    duration: const Duration(milliseconds: 375),
                    childAnimationBuilder: (widget) => SlideAnimation(
                      verticalOffset: -50.0,
                      child: FadeInAnimation(
                        child: widget,
                      ),
                    ),
                    children: [
                      Row(
                        children: [
                          Text('Humidity :',style: TextStyle(fontWeight: FontWeight.w500)),
                          SizedBox(width: 8,),
                          AnimatedTextWidget(text: '${widget.consolidatedWeather.humidity}%',dayId: widget.consolidatedWeather.id,),
                        ],
                      ),
                      const SizedBox(height: 16,),
                      Row(
                        children: [
                          Text('Pressure :',style: TextStyle(fontWeight: FontWeight.w500)),
                          SizedBox(width: 8,),
                          AnimatedTextWidget(text: '${widget.consolidatedWeather.airPressure} hPa',dayId: widget.consolidatedWeather.id),
                        ],
                      ),
                      const SizedBox(height: 16,),
                      Row(
                        children: [
                          Text('Wind :',style: TextStyle(fontWeight: FontWeight.w500)),
                          SizedBox(width: 8,),
                          AnimatedTextWidget(text : '${getSubStr(widget.consolidatedWeather.windSpeed.toString())} km/h',dayId: widget.consolidatedWeather.id),
                        ],
                      ),
                      const SizedBox(height: 16,)
                    ]
                ),
              ),
            ),
          ),
          SizedBox(height: 16,),
        ],
      ),
    );
  }


}
