import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_entity.dart';
import 'package:flaconi_weather_app/providers/setting_provider/setting_provider.dart';
import 'package:flaconi_weather_app/providers/weather_provider/Weather_provider.dart';
import 'package:flaconi_weather_app/resources/const_keeper.dart';
import 'package:flaconi_weather_app/utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';
import 'package:provider/provider.dart';

class WeatherItemWidget extends StatelessWidget {

  WeatherProvider weatherProvider;
  ConsolidatedWeather consolidatedWeather;
  WeatherItemWidget({Key? key,required this.weatherProvider,required this.consolidatedWeather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).bg1,
      borderRadius: const BorderRadius.all(Radius.circular(16)),
      child: InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(16)),
        onTap: (){
          weatherProvider.setWatherDetailItem = consolidatedWeather;
        },
        child: Container(
          width: 90,height: 90,
          alignment: Alignment.center,
          padding: const EdgeInsets.only(left: 8,right: 8),
          decoration: BoxDecoration(
            border: consolidatedWeather == weatherProvider.getWatherDetailItem ?  Border.all(color: Theme.of(context).primary,width: 1) : null,
            borderRadius: const BorderRadius.all(Radius.circular(16)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('${convertDateToDay(consolidatedWeather.applicableDate)}',maxLines: 1,style: TextStyle(fontSize: 12)),
              FadeInImage(
                  width: 30,height: 30,
                  placeholder: AssetImage('assets/images/place_holder.png',),
                  image: NetworkImage('${ConstKeeper.imageBaseUrl}${consolidatedWeather.weatherStateAbbr}.png')
              ),
              Text('${Provider.of<SettingProvider>(context).isFahrenheit ? getSubStr(convertCelsiusToFahrenheit(consolidatedWeather.minTemp).toString()) : getSubStr(consolidatedWeather.minTemp.toString())}'
                  '°/'
                  '${Provider.of<SettingProvider>(context).isFahrenheit ? getSubStr(convertCelsiusToFahrenheit(consolidatedWeather.maxTemp).toString()) : getSubStr(consolidatedWeather.maxTemp.toString())}°'
                ,maxLines: 1,style: TextStyle(fontSize: 12),),
            ],
          ),
        ),
      ),
    );
  }
}
