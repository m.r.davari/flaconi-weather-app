import 'dart:async';
import 'dart:ui';

import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_entity.dart';
import 'package:flaconi_weather_app/providers/setting_provider/setting_provider.dart';
import 'package:flaconi_weather_app/providers/weather_provider/Weather_provider.dart';
import 'package:flaconi_weather_app/resources/const_keeper.dart';
import 'package:flaconi_weather_app/utils/common_utils.dart';
import 'package:flaconi_weather_app/views/widgets/textfiels/animated_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:provider/provider.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';
import 'package:shimmer/shimmer.dart';

class WeatherDetailWidgetLandscape extends StatefulWidget {

  ConsolidatedWeather consolidatedWeather;
  String city;
   WeatherDetailWidgetLandscape({Key? key,required this.consolidatedWeather,required this.city}) : super(key: key);

  @override
  State<WeatherDetailWidgetLandscape> createState() => _WeatherDetailWidgetLandscapeState();
}


class _WeatherDetailWidgetLandscapeState extends State<WeatherDetailWidgetLandscape> with SingleTickerProviderStateMixin{


  bool shimmerEnable = true;


  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 800),(){
      setState(() {
        shimmerEnable = false;
      });
    });

  }



  @override
  void didUpdateWidget(WeatherDetailWidgetLandscape oldWidget) {
    super.didUpdateWidget(oldWidget);
    if(widget.consolidatedWeather.id != oldWidget.consolidatedWeather.id){
      setState(() {
        shimmerEnable = true;
      });
      Future.delayed(Duration(milliseconds: 800),(){
        setState(() {
          shimmerEnable = false;
        });
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return AnimationLimiter(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(width: 40,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: AnimationConfiguration.toStaggeredList(
              duration: const Duration(milliseconds: 375),
              childAnimationBuilder: (widget) => SlideAnimation(
                verticalOffset: -50.0,
                child: FadeInAnimation(
                  child: widget,
                ),
              ),
              children: [
                Row(
                  children: [
                    Text('${widget.city},',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),),
                    SizedBox(width: 8,),
                    AnimatedTextWidget(text: '${convertDateToDay(widget.consolidatedWeather.applicableDate)}',dayId: widget.consolidatedWeather.id,),
                  ],
                ),
                const SizedBox(height: 16,),
                Row(
                  children: [
                    Text('Status :',style: TextStyle(fontSize: 13,fontWeight: FontWeight.w500),),
                    SizedBox(width: 8,),
                    AnimatedTextWidget(text:'${widget.consolidatedWeather.weatherStateName}',dayId: widget.consolidatedWeather.id,),
                  ],
                ),
                const SizedBox(height: 16,),
                Row(
                  children: [
                    Text('Temperature :',style: TextStyle(fontSize: 13,fontWeight: FontWeight.w500),),
                    SizedBox(width: 8,),
                    AnimatedTextWidget(text: '${Provider.of<SettingProvider>(context).isFahrenheit ? getSubStr(convertCelsiusToFahrenheit(widget.consolidatedWeather.theTemp).toString(),end: 5)+' °F': getSubStr(widget.consolidatedWeather.theTemp.toString(),end: 5)+' °C'}',dayId: widget.consolidatedWeather.id,),
                  ],
                ),
                const SizedBox(height: 16,),
                Row(
                  children: [
                    Text('Humidity :',style: TextStyle(fontSize: 13,fontWeight: FontWeight.w500),),
                    SizedBox(width: 8,),
                    AnimatedTextWidget(text: '${widget.consolidatedWeather.humidity}%',dayId: widget.consolidatedWeather.id,),
                  ],
                ),
                const SizedBox(height: 16,),
                Row(
                  children: [
                    Text('Pressure :',style: TextStyle(fontSize: 13,fontWeight: FontWeight.w500),),
                    SizedBox(width: 8,),
                    AnimatedTextWidget(text: '${widget.consolidatedWeather.airPressure} hPa',dayId: widget.consolidatedWeather.id,),
                  ],
                ),
                const SizedBox(height: 16,),
                Row(
                  children: [
                    Text('wind :',style: TextStyle(fontSize: 13,fontWeight: FontWeight.w500),),
                    SizedBox(width: 8,),
                    AnimatedTextWidget(text: ' ${getSubStr(widget.consolidatedWeather.windSpeed.toString())} km/h',dayId: widget.consolidatedWeather.id,)
                  ],
                ),
                const SizedBox(height: 16,)
              ],
            ),
          ),
          Spacer(),
          Stack(
            alignment: Alignment.center,
            children: [
              FadeInImage(
                width: 200,height: 200,fit: BoxFit.contain,
                placeholder: AssetImage('assets/images/place_holder.png',),
                image: NetworkImage('${ConstKeeper.imageBaseUrl}${widget.consolidatedWeather.weatherStateAbbr}.png',),
              ),
              Visibility(
                visible: shimmerEnable,
                child: Shimmer.fromColors(child: FadeInImage(
                  width: 200,height: 200,fit: BoxFit.contain,
                  placeholder: AssetImage('assets/images/place_holder.png',),
                  image: NetworkImage('${ConstKeeper.imageBaseUrl}${widget.consolidatedWeather.weatherStateAbbr}.png'),
                ), baseColor: Colors.transparent, highlightColor: Colors.white,period: Duration(milliseconds: 800),enabled: shimmerEnable,),
              )

            ],
          ),
          SizedBox(width: 40,),
        ],
      ),
    );
  }


}
