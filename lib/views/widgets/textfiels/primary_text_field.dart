import 'package:flutter/material.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';

class PrimaryTextField extends StatefulWidget {
  String label;
  Function(String)? onChange;
  TextEditingController? textEditingController;
  FocusNode? focusNode;
  PrimaryTextField({Key? key,required this.label,this.onChange,this.textEditingController,this.focusNode}) : super(key: key);

  @override
  State<PrimaryTextField> createState() => _PrimaryTextFieldState();
}

class _PrimaryTextFieldState extends State<PrimaryTextField> {
  var fNode;
  var tEController;
  @override
  void initState() {
    fNode = widget.focusNode ?? FocusNode();
    tEController = widget.textEditingController ?? TextEditingController();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 16),
      height: 53,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Theme.of(context).textFieldUnFocusedBackgroundColor,
        border: Border.all(width: 0.7,color: fNode.hasFocus ?Theme.of(context).textFieldFocusedBorderColor : Theme.of(context).textFieldUnfocusedBorderColor),
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: TextField(
        controller: tEController,
        focusNode: fNode,
        onChanged: (text){
          widget.onChange??(text);
        },
        decoration: InputDecoration(
          label: Text('${widget.label}'),
          border: InputBorder.none,
          isDense: true,
          suffixIcon: Icon(Icons.search)
        ),
      ),
    );
  }
}
