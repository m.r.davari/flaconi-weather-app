

import 'package:flutter/material.dart';

class AnimatedTextWidget extends StatefulWidget {

  final String? text;
  final int? dayId;
  final TextStyle? textStyle;

  AnimatedTextWidget({this.text,this.dayId,this.textStyle,Key? key}) : super(key: key);

  @override
  _AnimatedTextWidgetState createState() => _AnimatedTextWidgetState();
}

class _AnimatedTextWidgetState extends State<AnimatedTextWidget> with TickerProviderStateMixin{

  Animation<double>? scaleAnimation;
  AnimationController? scaleController;
  String text = '';

  @override
  void initState() {
    text = widget.text!;
    super.initState();

    scaleController = new AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    scaleAnimation = new Tween<double>(begin: 1,end: 1.12).animate(scaleController!);

    scaleController!.addStatusListener((status){
      if(status == AnimationStatus.completed){
        scaleController!.reverse();
      }
    });

  }

  @override
  void dispose() {
    scaleController!.dispose();
    scaleController!.removeStatusListener((status) { });
    scaleAnimation!.removeListener(() { });;
    super.dispose();
  }


  @override
  void didUpdateWidget(covariant AnimatedTextWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    if(oldWidget.dayId!= widget.dayId || oldWidget.text != widget.text){
      scaleController!.forward();
      text = widget.text!;
      setState(()=>{});
    }

  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: AnimatedBuilder(
        animation: scaleController!,
        builder: (ctx,wid){
          return ScaleTransition(
            scale: scaleAnimation!,
            child: Text('$text',
                textDirection: TextDirection.ltr,
                style: widget.textStyle ?? TextStyle(fontSize: 14,)),
          );
        },
      ),
    );
  }
}