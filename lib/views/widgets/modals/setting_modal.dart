import 'package:flaconi_weather_app/providers/setting_provider/setting_provider.dart';
import 'package:flaconi_weather_app/providers/theme_provider/theme_provider.dart';
import 'package:flaconi_weather_app/resources/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';
import 'package:provider/provider.dart';

class SettingModal extends StatelessWidget {
  const SettingModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer2<ThemeProvider,SettingProvider>(
      builder: (ctx,themeData,settingData,_){
        return Container(
          decoration: BoxDecoration(
            color: Theme.of(context).bg,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(24),topRight: Radius.circular(24))
          ),
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: 6,width: 50,
                decoration: BoxDecoration(
                  color: Theme.of(context).dividerColor,
                  borderRadius: BorderRadius.circular(16),
                ),),
              const SizedBox(height: 16,),
              Text('Setting'),
              const SizedBox(height: 20,),
              Divider(
                height: 1,
                thickness: 0.5,
                color: Theme.of(context).dividerColor,
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      const SizedBox(height: 20,),
                      const Align(alignment: Alignment.centerLeft,child: Text('Theme Setting')),
                      const SizedBox(height: 16,),
                      Material(
                        color: Theme.of(context).bg1,
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        child: SwitchListTile(
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(16)),
                          ),
                          title: const Text('Light Mode'),
                          onChanged: (v){
                            themeData.setTheme(v ? AppTheme.Light : AppTheme.Dark);
                          },
                          value: themeData.getTheme.brightness == Brightness.light,
                        ),
                      ),
                      const SizedBox(height: 24,),
                      const Align(alignment: Alignment.centerLeft,child: Text('Temperature Setting')),
                      const SizedBox(height: 16,),
                      Material(
                        color: Theme.of(context).bg1,
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        child: SwitchListTile(
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(16)),
                          ),
                          tileColor: Theme.of(context).bg1,
                          title: const Text('Fahrenheit Mode'),
                          onChanged: (v){
                            settingData.setIsFahrenheit(v);
                          },
                          value: settingData.isFahrenheit,
                        ),
                      ),
                    ],
                  ),
                ),
              )

            ],
          ),
        );
      },
    );
  }
}
