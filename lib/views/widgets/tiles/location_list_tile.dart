import 'package:flaconi_weather_app/data/location/models/location_entity.dart';
import 'package:flaconi_weather_app/utils/route_transitions.dart';
import 'package:flaconi_weather_app/views/pages/weather_page.dart';
import 'package:flutter/material.dart';
import 'package:flaconi_weather_app/resources/theme/color_extension.dart';

class LocationListTile extends StatelessWidget {

  Location location;
  LocationListTile({Key? key,required this.location}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).bg1,
      borderRadius: const BorderRadius.all(Radius.circular(8)),
      child: InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(8)),
        onTap: (){
          Navigator.push(context, FadeRoute(page: WeatherPage(location: location,city: location.title)));
        },
        child: Container(
          padding: const EdgeInsets.all(16),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Location : ${location.title}'),
                  const SizedBox(height: 8,),
                  Text('Type : ${location.locationType}'),
                ],
              ),
              Spacer(),
              Icon(Icons.location_on_outlined,size: 21,)
            ],
          ),
        ),
      ),
    );
  }
}
