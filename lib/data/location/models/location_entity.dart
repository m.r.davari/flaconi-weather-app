import 'package:flaconi_weather_app/data/location/models/location_model.dart';

class Location {

  String title;
  String locationType;
  int woeid;
  String lattLong;

  Location(this.title, this.locationType, this.woeid, this.lattLong);

  factory Location.fromModel(LocationModel model) => Location(
        model.title ?? '',
        model.locationType ?? '',
        model.woeid ?? -1,
        model.lattLong ?? '',
      );

}
