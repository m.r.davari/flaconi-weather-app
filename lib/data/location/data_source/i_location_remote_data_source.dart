import 'package:flaconi_weather_app/data/location/models/location_model.dart';

abstract class ILocationRemoteDataSource{

  Future<List<LocationModel>> fetchSearch({Map<String, dynamic>? parameters});

}