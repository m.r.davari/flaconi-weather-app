
import 'package:flaconi_weather_app/core/network/i_request_manager.dart';
import 'package:flaconi_weather_app/data/location/models/location_model.dart';

import 'i_location_remote_data_source.dart';

class LocationRemoteDataSource extends ILocationRemoteDataSource{

  final IRequestManager requestManager;
  LocationRemoteDataSource({required this.requestManager});


  @override
  Future<List<LocationModel>> fetchSearch({Map<String, dynamic>? parameters})async{
    final result = await requestManager.getRequest(path: 'location/search',parameters: parameters);
    List<dynamic> resultList = result.data;
    return resultList.map((e) => LocationModel.fromJson(e)).toList();
  }
}
