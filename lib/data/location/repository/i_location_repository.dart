import 'package:flaconi_weather_app/data/location/models/location_entity.dart';

abstract class ILocationRepository{
  Future<List<Location>> callSearchLocation({Map<String, dynamic>? parameters});
}