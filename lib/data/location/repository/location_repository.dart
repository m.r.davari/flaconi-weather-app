import 'package:flaconi_weather_app/data/location/models/location_entity.dart';
import 'package:flaconi_weather_app/data/location/models/location_model.dart';
import 'package:flaconi_weather_app/data/location/data_source/location_remote_data_source.dart';

import '../data_source/i_location_remote_data_source.dart';
import 'i_location_repository.dart';
class LocationRepository extends ILocationRepository{

  final ILocationRemoteDataSource locastionRemoteDataSource;
  LocationRepository({required this.locastionRemoteDataSource});

  @override
  Future<List<Location>> callSearchLocation({Map<String, dynamic>? parameters}) async {
    var result = await locastionRemoteDataSource.fetchSearch(parameters:parameters);
    return result.map((e) => Location.fromModel(e)).toList();
  }
}