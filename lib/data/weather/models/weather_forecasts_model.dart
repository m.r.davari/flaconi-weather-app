class WeatherForecastsModel {
  List<ConsolidatedWeatherModel>? consolidatedWeather;
  String? time;
  String? sunRise;
  String? sunSet;
  String? timezoneName;
  ParentModel? parent;
  List<SourcesModel>? sources;
  String? title;
  String? locationType;
  int? woeid;
  String? lattLong;
  String? timezone;

  WeatherForecastsModel(
      {this.consolidatedWeather,
        this.time,
        this.sunRise,
        this.sunSet,
        this.timezoneName,
        this.parent,
        this.sources,
        this.title,
        this.locationType,
        this.woeid,
        this.lattLong,
        this.timezone});

  WeatherForecastsModel.fromJson(Map<String, dynamic> json) {
    if (json['consolidated_weather'] != null) {
      consolidatedWeather = <ConsolidatedWeatherModel>[];
      json['consolidated_weather'].forEach((v) {
        consolidatedWeather!.add(new ConsolidatedWeatherModel.fromJson(v));
      });
    }
    time = json['time'];
    sunRise = json['sun_rise'];
    sunSet = json['sun_set'];
    timezoneName = json['timezone_name'];
    parent =
    json['parent'] != null ? new ParentModel.fromJson(json['parent']) : null;
    if (json['sources'] != null) {
      sources = <SourcesModel>[];
      json['sources'].forEach((v) {
        sources!.add(new SourcesModel.fromJson(v));
      });
    }
    title = json['title'];
    locationType = json['location_type'];
    woeid = json['woeid'];
    lattLong = json['latt_long'];
    timezone = json['timezone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.consolidatedWeather != null) {
      data['consolidated_weather'] =
          this.consolidatedWeather!.map((v) => v.toJson()).toList();
    }
    data['time'] = this.time;
    data['sun_rise'] = this.sunRise;
    data['sun_set'] = this.sunSet;
    data['timezone_name'] = this.timezoneName;
    if (this.parent != null) {
      data['parent'] = this.parent!.toJson();
    }
    if (this.sources != null) {
      data['sources'] = this.sources!.map((v) => v.toJson()).toList();
    }
    data['title'] = this.title;
    data['location_type'] = this.locationType;
    data['woeid'] = this.woeid;
    data['latt_long'] = this.lattLong;
    data['timezone'] = this.timezone;
    return data;
  }
}

class ConsolidatedWeatherModel {
  int? id;
  String? weatherStateName;
  String? weatherStateAbbr;
  String? windDirectionCompass;
  String? created;
  String? applicableDate;
  double? minTemp;
  double? maxTemp;
  double? theTemp;
  double? windSpeed;
  double? windDirection;
  double? airPressure;
  int? humidity;
  double? visibility;
  int? predictability;

  ConsolidatedWeatherModel(
      {this.id,
        this.weatherStateName,
        this.weatherStateAbbr,
        this.windDirectionCompass,
        this.created,
        this.applicableDate,
        this.minTemp,
        this.maxTemp,
        this.theTemp,
        this.windSpeed,
        this.windDirection,
        this.airPressure,
        this.humidity,
        this.visibility,
        this.predictability});

  ConsolidatedWeatherModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    weatherStateName = json['weather_state_name'];
    weatherStateAbbr = json['weather_state_abbr'];
    windDirectionCompass = json['wind_direction_compass'];
    created = json['created'];
    applicableDate = json['applicable_date'];
    minTemp = json['min_temp'];
    maxTemp = json['max_temp'];
    theTemp = json['the_temp'];
    windSpeed = json['wind_speed'];
    windDirection = json['wind_direction'];
    airPressure = json['air_pressure'];
    humidity = json['humidity'];
    visibility = json['visibility'];
    predictability = json['predictability'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['weather_state_name'] = this.weatherStateName;
    data['weather_state_abbr'] = this.weatherStateAbbr;
    data['wind_direction_compass'] = this.windDirectionCompass;
    data['created'] = this.created;
    data['applicable_date'] = this.applicableDate;
    data['min_temp'] = this.minTemp;
    data['max_temp'] = this.maxTemp;
    data['the_temp'] = this.theTemp;
    data['wind_speed'] = this.windSpeed;
    data['wind_direction'] = this.windDirection;
    data['air_pressure'] = this.airPressure;
    data['humidity'] = this.humidity;
    data['visibility'] = this.visibility;
    data['predictability'] = this.predictability;
    return data;
  }
}

class ParentModel {
  String? title;
  String? locationType;
  int? woeid;
  String? lattLong;

  ParentModel({this.title, this.locationType, this.woeid, this.lattLong});

  ParentModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    locationType = json['location_type'];
    woeid = json['woeid'];
    lattLong = json['latt_long'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['location_type'] = this.locationType;
    data['woeid'] = this.woeid;
    data['latt_long'] = this.lattLong;
    return data;
  }
}

class SourcesModel {
  String? title;
  String? slug;
  String? url;
  int? crawlRate;

  SourcesModel({this.title, this.slug, this.url, this.crawlRate});

  SourcesModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    slug = json['slug'];
    url = json['url'];
    crawlRate = json['crawl_rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['slug'] = this.slug;
    data['url'] = this.url;
    data['crawl_rate'] = this.crawlRate;
    return data;
  }
}