import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_model.dart';

class WeatherForecasts {
  List<ConsolidatedWeather> consolidatedWeather;
  String time;
  String sunRise;
  String sunSet;
  String timezoneName;
  Parent parent;
  List<Sources> sources;
  String title;
  String locationType;
  int? woeid;
  String lattLong;
  String timezone;

  WeatherForecasts(
      this.consolidatedWeather,
        this.time,
        this.sunRise,
        this.sunSet,
        this.timezoneName,
        this.parent,
        this.sources,
        this.title,
        this.locationType,
        this.woeid,
        this.lattLong,
        this.timezone);


  
  factory WeatherForecasts.fromModel(WeatherForecastsModel model)=>WeatherForecasts(
    model.consolidatedWeather?.map((e) => ConsolidatedWeather.fromModel(e)).toList() ?? [],
    model.time ?? '',
    model.sunRise ?? '',
    model.sunSet ?? '',
    model.timezoneName ?? '',
    Parent.fromModel(model.parent!),
    model.sources?.map((e) => Sources.fromModel(e)).toList() ?? [],
    model.title??'',
    model.locationType?? '',
    model.woeid,
    model.lattLong ??'',
    model.timezone ?? ''
  );

}



class ConsolidatedWeather {
  int id;
  String weatherStateName;
  String weatherStateAbbr;
  String windDirectionCompass;
  String created;
  String applicableDate;
  double minTemp;
  double maxTemp;
  double theTemp;
  double windSpeed;
  double windDirection;
  double airPressure;
  int humidity;
  double visibility;
  int predictability;

  ConsolidatedWeather(
      this.id,
        this.weatherStateName,
        this.weatherStateAbbr,
        this.windDirectionCompass,
        this.created,
        this.applicableDate,
        this.minTemp,
        this.maxTemp,
        this.theTemp,
        this.windSpeed,
        this.windDirection,
        this.airPressure,
        this.humidity,
        this.visibility,
        this.predictability);

  
  factory ConsolidatedWeather.fromModel(ConsolidatedWeatherModel model)=>ConsolidatedWeather(
      model.id ?? -1,
      model.weatherStateName ?? '',
      model.weatherStateAbbr ?? '',
      model.windDirectionCompass ?? '',
      model.created ?? '',
      model.applicableDate ?? '',
      model.minTemp ?? 0.0,
      model.maxTemp ?? 0.0,
      model.theTemp?? 0.0,
      model.windSpeed ?? 0.0,
      model.windDirection ?? 0.0,
      model.airPressure ?? 0.0,
      model.humidity?? -1,
      model.visibility ?? 0.0,
      model.predictability ?? -1
  );
  
}

class Parent {
  String title;
  String locationType;
  int woeid;
  String lattLong;

  Parent(this.title, this.locationType, this.woeid, this.lattLong);

  factory Parent.fromModel(ParentModel model)=>Parent(
      model.title ?? '',
      model.locationType ?? '',
      model.woeid ?? -1,
      model.lattLong ?? ''
  );
}

class Sources {
  String title;
  String slug;
  String url;
  int crawlRate;

  Sources(this.title, this.slug, this.url, this.crawlRate);

  factory Sources.fromModel(SourcesModel model)=>Sources(
      model.title ?? '',
      model.slug ?? '',
      model.url ?? '',
      model.crawlRate ?? -1
  );
}

