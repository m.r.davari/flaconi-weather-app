import 'package:flaconi_weather_app/data/weather/data_source/i_weather_remote_data_source.dart';
import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_entity.dart';
import 'package:flaconi_weather_app/data/weather/repository/i_weather_repository.dart';

class WeatherRepository extends IWeatherRepository{

  final IWeatherRemoteDataSource weatherRemoteDataSource;
  WeatherRepository({required this.weatherRemoteDataSource});

  @override
  Future<WeatherForecasts> callWeatherForecast({int? weoid})async{
    var result = await weatherRemoteDataSource.fetchWeatherForcast(weoid: weoid);
    return WeatherForecasts.fromModel(result);
  }
}