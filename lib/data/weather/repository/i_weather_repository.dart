import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_entity.dart';

abstract class IWeatherRepository {

  Future<WeatherForecasts> callWeatherForecast ({int? weoid});

}