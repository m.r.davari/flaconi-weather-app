
import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_model.dart';

abstract class IWeatherRemoteDataSource {

  Future<WeatherForecastsModel> fetchWeatherForcast ({int? weoid});

}