import 'package:flaconi_weather_app/core/network/i_request_manager.dart';
import 'package:flaconi_weather_app/data/location/data_source/i_location_remote_data_source.dart';
import 'package:flaconi_weather_app/data/location/models/location_model.dart';
import 'package:flaconi_weather_app/data/weather/data_source/i_weather_remote_data_source.dart';
import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_model.dart';

class WeatherRemoteDataSource extends IWeatherRemoteDataSource {

  final IRequestManager requestManager;
  WeatherRemoteDataSource({required this.requestManager});

  @override
  Future<WeatherForecastsModel> fetchWeatherForcast({int? weoid})async{
    var result =  await requestManager.getRequest(path: 'location/$weoid');
    return WeatherForecastsModel.fromJson(result.data);
  }

}