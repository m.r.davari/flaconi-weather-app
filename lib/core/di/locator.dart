
import 'package:flaconi_weather_app/core/network/dio_request_manager.dart';
import 'package:flaconi_weather_app/core/network/i_request_manager.dart';
import 'package:flaconi_weather_app/data/location/data_source/i_location_remote_data_source.dart';
import 'package:flaconi_weather_app/data/location/repository/i_location_repository.dart';
import 'package:flaconi_weather_app/data/location/repository/location_repository.dart';
import 'package:flaconi_weather_app/data/location/data_source/location_remote_data_source.dart';
import 'package:flaconi_weather_app/data/weather/data_source/i_weather_remote_data_source.dart';
import 'package:flaconi_weather_app/data/weather/data_source/weather_remote_data_source.dart';
import 'package:flaconi_weather_app/data/weather/repository/i_weather_repository.dart';
import 'package:flaconi_weather_app/data/weather/repository/weather_repository.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator(){

  locator.registerSingleton<IRequestManager>(DioRequestManager());
  
  locator.registerSingleton<ILocationRemoteDataSource>(LocationRemoteDataSource(requestManager: locator<IRequestManager>()));

  locator.registerSingleton<ILocationRepository>(LocationRepository(locastionRemoteDataSource: locator<ILocationRemoteDataSource>()));

  locator.registerSingleton<IWeatherRemoteDataSource>(WeatherRemoteDataSource(requestManager: locator<IRequestManager>()));

  locator.registerSingleton<IWeatherRepository>(WeatherRepository(weatherRemoteDataSource: locator<IWeatherRemoteDataSource>()));

}