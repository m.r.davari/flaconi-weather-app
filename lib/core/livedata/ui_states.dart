
abstract class UIState<S>{

}

class Initial<I> extends UIState<I>{}

class IsLoading<L> extends UIState<L>{}

class Success<D> extends UIState<D>{
  D data;
  Success(this.data);
}

class Failure<E> extends UIState<E>{
  String error;
  Failure(this.error);
}

