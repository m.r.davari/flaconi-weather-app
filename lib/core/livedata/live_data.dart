
import 'package:flaconi_weather_app/core/livedata/ui_states.dart';
import 'package:flutter/material.dart';


class LiveData <V extends UIState>{
  late V value;

  void setValue(V value){
    this.value = value;
  }

  UIState getValue(){
    return this.value;
  }
}