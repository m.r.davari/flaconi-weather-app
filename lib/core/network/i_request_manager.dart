
abstract class IRequestManager{

  Future<dynamic> getRequest ({String? path, Map<String, dynamic>? parameters});

  Future<dynamic> postRequest({String? path, var body});


}