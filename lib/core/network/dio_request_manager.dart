
import 'package:dio/dio.dart';
import 'package:flaconi_weather_app/core/network/i_request_manager.dart';
import 'package:flaconi_weather_app/resources/const_keeper.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class DioRequestManager extends IRequestManager {

  late Dio _dio;
  late BaseOptions _baseOptions;
  final int _connectTimeout = 12000;
  final int _receiveTimeout = 10000;

  DioRequestManager(){

    _baseOptions = BaseOptions(
      baseUrl: ConstKeeper.baseUrl,
      connectTimeout: _connectTimeout,
      receiveTimeout: _receiveTimeout
    );
    _dio = Dio(_baseOptions);


    setUpInterceptor();

  }

  void setUpInterceptor(){
    _dio.interceptors.add(
        InterceptorsWrapper(
          onRequest: (options, handler) {
            options.headers = {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'accept-charset': 'UTF-8',
            };
            return handler.next(options);
          },
          onResponse: (response, handler) {
            return handler.next(response);
          },
          onError: (error, handler) {
            switch(error.type){
              case DioErrorType.connectTimeout :
                return handler.next(
                  DioError(
                    requestOptions: error.requestOptions,
                    error: 'Connection Time Out',
                    response: error.response
                  )
                );
              case DioErrorType.receiveTimeout :
                return handler.next(
                    DioError(
                        requestOptions: error.requestOptions,
                        error: 'Connection Time Out',
                        response: error.response
                    )
                );
              case DioErrorType.sendTimeout :
                return handler.next(
                    DioError(
                        requestOptions: error.requestOptions,
                        error: 'Send Time Out',
                        response: error.response
                    )
                );
              case DioErrorType.cancel :
                return handler.next(
                    DioError(
                        requestOptions: error.requestOptions,
                        error: 'Request Canceled',
                        response: error.response
                    )
                );
              case DioErrorType.other :
                return handler.next(
                    DioError(
                        requestOptions: error.requestOptions,
                        error: 'Something Went Wrong',
                        response: error.response
                    )
                );
              case DioErrorType.response :
                return handler.next(
                    DioError(
                        requestOptions: error.requestOptions,
                        error: '${error.response!.statusMessage}',
                        response: error.response
                    )
                );
            }
            return handler.next(error);
          },
        )
    );


    // _dio.interceptors.add(PrettyDioLogger(
    //     requestHeader: true,
    //     requestBody: true,
    //     responseBody: true,
    //     responseHeader: true,
    //     error: true,
    //     compact: true,
    //     maxWidth: 90));


  }

  @override
  Future<dynamic> getRequest({String? path, Map<String, dynamic>? parameters})async{
    try{
      return await _dio.get(path!,queryParameters: parameters);
    }
    on DioError catch (error){
      rethrow;
    }
  }

  @override
  Future<dynamic> postRequest({String? path, var body})async{
    try{
      return await _dio.post(path!,data: body);
    }
    on DioError catch (error){
      throw error;
    }
  }

}