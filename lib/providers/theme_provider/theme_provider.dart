import 'package:flaconi_weather_app/resources/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeProvider with ChangeNotifier {
  ThemeData? _themeData;

  final _preferenceThemeKey = "ThemeKey";


  Future<void> loadTheme() async {
    var prefs = await SharedPreferences.getInstance();
    int preferredTheme = prefs.getInt(_preferenceThemeKey) ?? 0;
    print('-------- ${AppTheme.values[preferredTheme]}');
    _themeData = appThemeData[AppTheme.values[preferredTheme]]!;
  }

  ThemeData get getTheme {
    _themeData ??= appThemeData[AppTheme.Light]!;
    return _themeData!;
  }

  setTheme(AppTheme theme) async {
    _themeData = appThemeData[theme]!;
    var prefs = await SharedPreferences.getInstance();
    await prefs.setInt(_preferenceThemeKey, AppTheme.values.indexOf(theme));
    notifyListeners();
  }
}
