import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';


class SettingProvider with ChangeNotifier{
  late bool _isFahrenheit;

  final _preferenceTemperatureKey = "TemperatureKey";


  Future<void> loadIsFahrenheit() async {
    var prefs = await SharedPreferences.getInstance();
    bool isFahrenheitPref = prefs.getBool(_preferenceTemperatureKey) ?? false;
    _isFahrenheit = isFahrenheitPref;
  }


  bool get isFahrenheit => _isFahrenheit;


  setIsFahrenheit(bool isFahrenheitValue) async {
    _isFahrenheit = isFahrenheitValue;
    var prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_preferenceTemperatureKey, isFahrenheitValue);
    notifyListeners();
  }

}