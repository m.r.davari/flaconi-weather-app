import 'package:dio/dio.dart';
import 'package:flaconi_weather_app/core/di/locator.dart';
import 'package:flaconi_weather_app/core/livedata/live_data.dart';
import 'package:flaconi_weather_app/core/livedata/ui_states.dart';
import 'package:flaconi_weather_app/data/location/models/location_entity.dart';
import 'package:flaconi_weather_app/data/location/repository/i_location_repository.dart';
import 'package:flaconi_weather_app/data/location/repository/location_repository.dart';
import 'package:flaconi_weather_app/data/location/models/location_model.dart';
import 'package:flutter/cupertino.dart';

class LocationProvider with ChangeNotifier{

  ILocationRepository locationRepository;
  LocationProvider(this.locationRepository);

  LiveData<UIState<List<Location>>> _locatiosLiveData = LiveData<UIState<List<Location>>>();

  LiveData<UIState<List<Location>>> get getLocatiosLiveData => _locatiosLiveData;


  Future<void> searchLocation({String? city})async {
    _locatiosLiveData.setValue(IsLoading());
    notifyListeners();
    try{
      Map<String,dynamic> params = {
        'query':'$city'
      };
      final List<Location> locations = await locationRepository.callSearchLocation(parameters:params);
      _locatiosLiveData.setValue(Success(locations));
    }
    on DioError catch(err){
      print('errrrrrrrr ${err.toString()}');
      _locatiosLiveData.setValue(Failure(err.message));
    }
    catch(e){
      print('errrrrrrrrzaa ${e.toString()}');
    }
    finally{
      notifyListeners();
    }
  }

}