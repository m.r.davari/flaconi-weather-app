

import 'package:dio/dio.dart';
import 'package:flaconi_weather_app/core/livedata/live_data.dart';
import 'package:flaconi_weather_app/core/livedata/ui_states.dart';
import 'package:flaconi_weather_app/data/weather/models/weather_forecasts_entity.dart';
import 'package:flaconi_weather_app/data/weather/repository/i_weather_repository.dart';
import 'package:flutter/cupertino.dart';

class WeatherProvider with ChangeNotifier{

  IWeatherRepository weatherRepository;
  WeatherProvider(this.weatherRepository);

  LiveData<UIState<WeatherForecasts>> _weatherForecastLiveData = LiveData<UIState<WeatherForecasts>>();

  LiveData<UIState<WeatherForecasts>> get getWeatherForecastLiveData => _weatherForecastLiveData;

  late ConsolidatedWeather _watherDetailItem;

  ConsolidatedWeather get getWatherDetailItem => _watherDetailItem;

  set setWatherDetailItem(ConsolidatedWeather value) {
    _watherDetailItem = value;
    notifyListeners();
  }

  Future<void> getWeatherForeCast(int weoid)async{
    _weatherForecastLiveData.setValue(IsLoading());
    notifyListeners();
    try{
      WeatherForecasts data = await weatherRepository.callWeatherForecast(weoid: weoid);
      _watherDetailItem = data.consolidatedWeather[0];
      _weatherForecastLiveData.setValue(Success(data));
    }
    on DioError catch (error){
      _weatherForecastLiveData.setValue(Failure(error.message));
    }
    finally{
      notifyListeners();
    }
  }

}