
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';




void hideKeyboard() {
  SystemChannels.textInput.invokeMethod('TextInput.hide');
}

void hideKeyboardFocus(BuildContext ctx) {
  SystemChannels.textInput.invokeMethod('TextInput.hide');
  FocusScope.of(ctx).requestFocus(new FocusNode());
}


String convertDateToDay (String dateFormatStr){

  try{
    DateFormat dateFormat = DateFormat("EEEE");
    String stringDate = dateFormat.format(DateTime.parse(dateFormatStr));
    return stringDate;
  }
  catch (exc) {
    return "N/A";
  }

}

String getSubStr(String str,{int? start=0,int? end=4}){
  if(str.length<end!){
    return str;
  }
  return str.substring(start!,end);
}


double convertCelsiusToFahrenheit (double value) {
  return (value * (9 / 5)) + 32;
}


double convertFahrenheitToCelsius(double value) {
  return (value - 32) * (5 / 9);
}