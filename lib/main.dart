import 'package:flaconi_weather_app/core/di/locator.dart';
import 'package:flaconi_weather_app/providers/setting_provider/setting_provider.dart';
import 'package:flaconi_weather_app/providers/theme_provider/theme_provider.dart';
import 'package:flaconi_weather_app/views/pages/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();

  ThemeProvider _themeProvider = ThemeProvider();
  await _themeProvider.loadTheme();

  SettingProvider _settingProvider = SettingProvider();
  await _settingProvider.loadIsFahrenheit();

  runApp(MyApp(themeProvider: _themeProvider,settingProvider: _settingProvider,));
}

class MyApp extends StatelessWidget {

  ThemeProvider themeProvider;
  SettingProvider settingProvider;
  MyApp({Key? key,required this.themeProvider,required this.settingProvider}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeProvider>(create: (ctx)=>themeProvider),
        ChangeNotifierProvider<SettingProvider>(create: (ctx)=>settingProvider),
      ],
      child: Consumer2<ThemeProvider,SettingProvider>(
        builder: (ctx,themeData,settingData,_){
          return MaterialApp(
            title: 'Flaconi Weather App',
            theme: themeData.getTheme,
            home: const SplashPage(),
          );
        },
      ),
    );

  }
}
